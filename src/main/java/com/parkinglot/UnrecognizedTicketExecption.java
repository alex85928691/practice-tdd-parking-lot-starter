package com.parkinglot;

public class UnrecognizedTicketExecption extends RuntimeException {

    public UnrecognizedTicketExecption(String message) {
        super(message);
    }
}
