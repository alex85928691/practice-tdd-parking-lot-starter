package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy{
    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public ParkingTicket park(Car car){
        ParkingLot parkingLot = parkingLots.stream().filter(a -> !a.isFull() || a.getInitialCapacity() !=0)
                .max(Comparator.comparing(a ->(float) a.checkCapacity()/ a.getInitialCapacity())).orElse(null);
        if(parkingLot != null && !parkingLot.isFull()){
            return parkingLot.park(car);
        }
        throw new NoavailablepositionExecption("No available position.");
    }
}
