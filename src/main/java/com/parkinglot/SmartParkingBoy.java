package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy extends ParkingBoy{
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public ParkingTicket park(Car car){
        ParkingLot morespaces = parkingLots.stream().
                max(Comparator.comparing(ParkingLot::checkCapacity)).orElse(null);
        if (!morespaces.isFull() && morespaces != null){
            return morespaces.park(car);
        }
        throw new NoavailablepositionExecption("No available position.");
    }


}
