package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private int capacity = 10;
    private Map<ParkingTicket, Car> ticketCarMap = new HashMap<>();

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public ParkingLot(){}




    public ParkingTicket park(Car car) {
        if (isFull()) {
            throw new NoavailablepositionExecption("No available position");
        }
        ParkingTicket ticket = new ParkingTicket();
        ticketCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        if(!ticketCarMap.containsKey(ticket)){
            throw new UnrecognizedTicketExecption("Unrecognized parking ticket.");
        }
        Car car = ticketCarMap.get(ticket);
        ticketCarMap.remove(ticket);
        return car;
    }

    public boolean isFull(){
        return ticketCarMap.size() >= capacity;
    }

    public boolean isValid(ParkingTicket ticket){
        return ticketCarMap.containsKey(ticket);
    }

    public int checkCapacity(){
        return this.capacity - this.ticketCarMap.size();
    }

    public int getInitialCapacity(){ return this.capacity;}
}
