package com.parkinglot;

import java.util.List;

public class ParkingBoy{
    protected List<ParkingLot> parkingLots;

    public ParkingBoy(List<ParkingLot> parkingLot){
        this.parkingLots = parkingLot;
    }


    public ParkingTicket park(Car car) {
        for(ParkingLot parkinglot : parkingLots){
            if(!parkinglot.isFull())
            return parkinglot.park(car);
        }
        throw new NoavailablepositionExecption("No available position");
    }

    public Car fetch(ParkingTicket ticket) {
        for(ParkingLot parkingLot : parkingLots){
            if(parkingLot.isValid(ticket)){
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketExecption("Unrecognized parking ticket.");
    }
}


