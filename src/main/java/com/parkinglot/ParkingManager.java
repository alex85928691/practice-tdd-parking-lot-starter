package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingManager extends ParkingBoy{
    private List<ParkingBoy> parkingBoys = new ArrayList<>();
    public ParkingManager(List<ParkingLot> parkingLot) {
        super(parkingLot);
    }

    public void addBoy(ParkingBoy parkingBoy){
        if(!parkingBoys.contains(parkingBoy)){
            parkingBoys.add(parkingBoy);
        }
    }

    public ParkingTicket orderBoy(Car car) {
        ParkingBoy boy = parkingBoys.stream().filter(parkingBoy -> !parkingBoy.parkingLots.
                isEmpty()).findFirst().orElse(null);

        if(boy != null && !boy.parkingLots.contains(car)){
            return boy.park(car);
        }
        throw new OrderBoyFailExecption("Order Boys Fail.");
    }

    public Car fetchByBoys(ParkingTicket ticket) {
        ParkingBoy boy = parkingBoys.stream().filter(parkingBoy->parkingBoy.parkingLots.stream().
                anyMatch(parkingLot ->parkingLot.isValid(ticket))).
                findFirst().orElse(null);

        if(boy !=null){
            return boy.fetch(ticket);
        }
        throw new TicketErrorExecption("Ticket does exist.");
    }
}
