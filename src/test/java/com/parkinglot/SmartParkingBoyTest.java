package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    
    @Test
    void should_return_second_parkinglot_ticket_when_park_given_second_parkinglot_has_more_spaces() {
            ParkingLot parkingLot1 = new ParkingLot(1);
            ParkingLot parkingLot2 = new ParkingLot(2);

            SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
            Car car = new Car();
            ParkingTicket ticket  = parkingboy.park(car);

            assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_return_first_parkinglot_ticket_when_park_given_same_parkinglot__spaces() {
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);

        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket  = parkingboy.park(car);

        assertEquals(car, parkingLot1.fetch(ticket));
    }

    @Test
    void should_return_second_parkinglot_ticket_when_park_given_second_and_thrid_parkinglot_more_spaces() {

        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);

        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket  = parkingboy.park(car);

        assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_return_right_car_when_fetch_given_two_parking_ticket() {

        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);


        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket  = parkingboy.park(car);
        Car car1 = new Car();
        ParkingTicket ticket1 = parkingboy.park(car1);


        assertEquals(car, parkingLot1.fetch(ticket));
        assertEquals(car1, parkingLot2.fetch(ticket1));
    }

    @Test
    void should_return_error_when_fetch_given_wrong_parking_ticket() {

        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);


        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        parkingboy.park(car);
        ParkingTicket ticket = new ParkingTicket();

        UnrecognizedTicketExecption error = assertThrows(UnrecognizedTicketExecption.class, ()->{
            parkingLot1.fetch(ticket);
        });
        assertEquals("Unrecognized parking ticket.", error.getMessage());

    }

    @Test
    void should_return_error_when_fetch_given_used_parking_ticket() {

        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);


        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket = parkingboy.park(car);
        parkingboy.fetch(ticket);


        UnrecognizedTicketExecption error = assertThrows(UnrecognizedTicketExecption.class, ()->{
            parkingLot1.fetch(ticket);
        });
        assertEquals("Unrecognized parking ticket.", error.getMessage());

    }

    @Test
    void should_return_error_when_park_given_two_parkinglots_are_full() {

        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);


        SmartParkingBoy parkingboy = new SmartParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();



        NoavailablepositionExecption error = assertThrows(NoavailablepositionExecption.class, ()->{
            parkingboy.park(car);
        });
        assertEquals("No available position.", error.getMessage());

    }


}
