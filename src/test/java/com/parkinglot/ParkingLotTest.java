package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


//case 1: given a parking lot, when park the car, then return a parking ticket.
//case 2：given a parking lot with parked car, and a parking ticket, When fetch the car,
//Then return the parked car.
//case 3: given a parking lot with two parked cars, and two parking tickets, when fetch the car
//twice , Then return the right car with each ticket.
//case 4: given a parking lot, and a wrong parking ticket, when fetch the car , then return nothing
//case 5: given a parking lot , and a used parking ticket, when fetch the car, then return nothing.
//case 6: given a parking lot without any position, and a car , when park the car , then return nothing.
public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parkinglot_and_car() {
        //given
        ParkingLot parkinglot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket parkingticket = parkinglot.park(car);
        //then
        assertNotNull(parkingticket);
    }


    //case 2：given a parking lot with parked car, and a parking ticket, When fetch the car,
    //Then return the parked car.
    @Test
    void should_return_parked_car_when_fetch_the_car_given_parking_ticket() {
        // Arrange
        ParkingLot parkingLot = new ParkingLot();
        Car expectedCar = new Car();
        ParkingTicket ticket = parkingLot.park(expectedCar);

        // Act
        Car actualCar = parkingLot.fetch(ticket);

        // Assert
        assertEquals(expectedCar, actualCar);
    }

    @Test
    void should_return_right_car_when_fetching_two_cars_with_two_tickets() {
        // Arrange
        ParkingLot parkingLot = new ParkingLot();
        Car expectedCar1 = new Car();
        Car expectedCar2 = new Car();
        ParkingTicket ticket1 = parkingLot.park(expectedCar1);
        ParkingTicket ticket2 = parkingLot.park(expectedCar2);

        // Act
        Car actualCar1 = parkingLot.fetch(ticket1);
        Car actualCar2 = parkingLot.fetch(ticket2);

        // Assert
        assertEquals(expectedCar1, actualCar1);
        assertEquals(expectedCar2, actualCar2);
    }

    //case 4: given a parking lot, and a wrong parking ticket, when fetch the car , then return exception
    @Test
    void should_nothing_when_fetch_given_wrong_parkinglot_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket correct = parkingLot.park(car);
        ParkingTicket wrong = new ParkingTicket();


        //then
        assertThrows(UnrecognizedTicketExecption.class,()->{
            parkingLot.fetch(wrong);
        });
    }

    //case 5: given a parking lot , and a used parking ticket, when fetch the car, then return execption.
    @Test
    void should_return_nothing_when_fetch_given_parkinglot_and_usedticket() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket used = parkingLot.park(car);

        Car fetched = parkingLot.fetch(used);



        //then
        assertThrows(UnrecognizedTicketExecption.class,()->{
            parkingLot.fetch(used);
        });
    }

    //case 6: given a parking lot without any position, and a car , when park the car , then return execption.

    @Test
    void should_return_nothing_when_park_given_parkinglot_no_position() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        parkingLot.park(car);
        parkingLot.park(car1);

        //then

        NoavailablepositionExecption execption= assertThrows(NoavailablepositionExecption.class,()->{
            parkingLot.park(car2);
        });

        assertEquals("No available position", execption.getMessage());
        //No available position
    }

}
