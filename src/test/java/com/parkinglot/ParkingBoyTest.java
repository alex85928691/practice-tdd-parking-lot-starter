package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
//given a standard parking boy , who manage two parking lots,both with available position, and a car, when park the car , then
// the car will be parked to the first parking lot.
    @Test
    void should_return_parkinglot1_ticket_when_park_given_2parkinglots_parkingBoy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingboy = new ParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        //when
        ParkingTicket parkingticket = parkingboy.park(car);
        //then
        assertEquals(car,parkingLot1.fetch(parkingticket));

    }

    @Test
    void should_return_parkinglot2_car_when_park_the_car_given_parkingboy_2parkinglots_packlot1isFull() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);

        ParkingBoy parkingboy = new ParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket = parkingboy.park(car);

        // Assert
        assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_return_right_car_when_park_the_car_given_parkingboy_2parkinglots_2packingtickets(){
        // Arrange
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        ParkingTicket ticket1 = parkingLot1.park(car1);
        ParkingTicket ticket2 = parkingLot2.park(car2);
        // when

        // then
        assertEquals(car1, parkingBoy.fetch(ticket1));
        assertEquals(car2, parkingBoy.fetch(ticket2));

    }

    @Test
    void should_return_error_when_fetch_the_car_given_parkingboy_2parkinglots_unrecongizedticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingboy = new ParkingBoy(List.of(parkingLot1,parkingLot2));
        ParkingTicket ticket = new ParkingTicket();

        //then
        assertThrows(UnrecognizedTicketExecption.class,()->{
            parkingboy.fetch(ticket);
        });
    }

    @Test
    void should_return_error_when_fetch_the_car_given_parkingboy_2parkinglots_usedticket() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingboy = new ParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket =  parkingboy.park(car);
        Car car1  =  parkingboy.fetch(ticket);

        //then
        assertThrows(UnrecognizedTicketExecption.class,()->{
            parkingboy.fetch(ticket);
        });
    }

    @Test
    void should_return_error_when_park_given_parkinglot_no_position_parkingboy_2parkinglots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);

        ParkingBoy parkingboy = new ParkingBoy(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        Car car1 = new Car();

        parkingboy.park(car);
        parkingboy.park(car1);


        NoavailablepositionExecption execption= assertThrows(NoavailablepositionExecption.class,()->{
            parkingboy.park(new Car());
        });
        assertEquals("No available position", execption.getMessage());

    }


}
