package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingManagerTest {

    @Test
    void should_return_parkinglot1_ticket_when_park_given_2parkinglots_parkingmanager_and_car() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingManager parkingboy = new ParkingManager(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        //when
        ParkingTicket parkingticket = parkingboy.park(car);
        //then
        assertEquals(car,parkingLot1.fetch(parkingticket));
    }

    @Test
    void should_return_parkinglot2_car_when_park_the_car_given_parkingmanager_2parkinglots_packlot1isFull() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);

        ParkingManager parkingboy = new ParkingManager(List.of(parkingLot1,parkingLot2));
        Car car = new Car();
        ParkingTicket ticket = parkingboy.park(car);

        // Assert
        assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_assign_correct_boy_when_park_given_3_boys_2_parkinglot() {
        //given
        ParkingLot parkinglot1 = new ParkingLot();
        ParkingLot parkinglot2 = new ParkingLot();
        ParkingBoy parkingboy = new ParkingBoy(List.of());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkinglot1));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkinglot2));
        ParkingManager parkingManager = new ParkingManager(List.of(parkinglot1,parkinglot2));

        //when
        Car car = new Car();
        parkingManager.addBoy(parkingboy);
        parkingManager.addBoy(smartParkingBoy);
        parkingManager.addBoy(superSmartParkingBoy);

        ParkingTicket ticket = parkingManager.orderBoy(car);

        //then
        assertEquals(car, smartParkingBoy.fetch(ticket));
    }

    @Test
    void should_return_car_when_fetch_given_given_3_boys_2_parkinglot() {
        ParkingLot parkinglot1 = new ParkingLot();
        ParkingLot parkinglot2 = new ParkingLot();
        ParkingBoy parkingboy = new ParkingBoy(List.of());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkinglot1));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkinglot2));
        ParkingManager parkingManager = new ParkingManager(List.of(parkinglot1,parkinglot2));
        //when

        parkingManager.addBoy(parkingboy);
        parkingManager.addBoy(smartParkingBoy);
        parkingManager.addBoy(superSmartParkingBoy);

        Car car = new Car();
        ParkingTicket ticket = smartParkingBoy.park(car);

        //when
        Car fetchedCar = parkingManager.fetchByBoys(ticket);

        //then
        assertEquals(car, fetchedCar);
    }
    
    @Test
    void should_error_when_park_given_parkingboy_no_parkinglot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingboy = new ParkingBoy(List.of());
        ParkingManager parkingManager = new ParkingManager(List.of(parkingLot1,parkingLot2));
        //when
        parkingManager.addBoy(parkingboy);

        Car car = new Car();

        //then
        OrderBoyFailExecption error = assertThrows(OrderBoyFailExecption.class,()->{
            parkingManager.orderBoy(car);
        });

        assertEquals("Order Boys Fail.",error.getMessage());
    }

    @Test
    void should_error_when_fetch_given_parkingboy_no_parkinglot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingboy = new ParkingBoy(List.of());
        ParkingManager parkingManager = new ParkingManager(List.of(parkingLot1,parkingLot2));
        //when
        parkingManager.addBoy(parkingboy);

        ParkingTicket ticket = new ParkingTicket();

        //then
        TicketErrorExecption error = assertThrows(TicketErrorExecption.class,()->{
            parkingManager.fetchByBoys(ticket);
        });

        assertEquals("Ticket does exist.",error.getMessage());
    }


}
